 // Підключення модуля HTTP
const https = require('https');

// Адреса веб-сторінки для перевірки
const url = 'https://kreik2017.github.io/PPPI.github.io/';

// Виклик HTTP запиту до веб-сторінки
https.get(url, (res) => {
    // Перевірка статусного коду відповіді
    if (res.statusCode === 200) {
        console.log('Тест пройшов успішно: веб-сторінка завантажилася.');
    } else {
        console.error(`Тест не вдалий: веб-сторінка не завантажилася, отримано статусний код ${res.statusCode}.`);
    }
}).on('error', (err) => {
    console.error('Виникла помилка під час виконання тесту:', err);
});